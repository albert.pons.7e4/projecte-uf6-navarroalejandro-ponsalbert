--creacio de les taules
CREATE TABLE teams (
    team_id SERIAL PRIMARY KEY,
    name VARCHAR(20),
    abv VARCHAR(3)
);

CREATE TABLE matches(
    match_id SERIAL,
    local_name VARCHAR(20),
    local_goals NUMERIC(3),
    visitor_name VARCHAR(20),
    visitor_goals NUMERIC(3)
);

--sentencies de manipulacio o filtracio de dades de la bdd
-- TEAMS
select * from teams order by team_id;
select * from teams order by team_id DESC;
select * from teams order by name;
select * from teams order by name DESC;
select * from teams order by abv;
select * from teams order by abv DESC;

-- MATCHES
select * from matches order by match_id;
select * from matches order by match_id DESC;
select * from matches order by local_name;
select * from matches order by local_name DESC;
select * from matches order by local_goals;
select * from matches order by local_goals DESC;
select * from matches order by visitor_name;
select * from matches order by visitor_name DESC;
select * from matches order by visitor_goals;
select * from matches order by visitor_goals DESC;
select m.match_id, m.local_name, m.local_goals, t.team_id, m.visitor_name, m.visitor_goals, t2.team_id from matches m, teams t, teams t2
where local_name = t.name AND visitor_name = t2.name;


-- MATCHES & TEAMS
-- Equip local y visitant amb més gols
select match_id, local_name, local_goals from matches where local_goals = (SELECT max(local_goals) from matches);
select match_id, visitor_name, visitor_goals from matches where visitor_goals = (SELECT max(visitor_goals) from matches);

-- Vitories dels equips locals
-- para saber el que tiene mas victorias añadir "ORDER BY 3 DESC LIMIT 1"
select distinct(m.local_name), t.team_id, count(local_name) as "Wins as Local"
from matches m, teams t
where m.local_goals > m.visitor_goals AND m.local_name = t.name
GROUP BY m.local_name, team_id;
--ORDER BY "Wins as Local" DESC LIMIT 1;

-- Victories dels equips visitants
-- para saber el que tiene mas victorias añadir "ORDER BY 3 DESC LIMIT 1"
select distinct(m.visitor_name), t.team_id, count(visitor_name) as "Wins as Visitor"
from matches m, teams t
where m.local_goals < m.visitor_goals AND m.visitor_name = t.name
GROUP BY m.visitor_name, team_id;
--ORDER BY "Wins as Visitor" DESC LIMIT 1;