package models;

public class Match {
    //variables de la classe
    private int id;
    private String local_name;
    private int local_goals;
    private String visitor_name;
    private int visitor_goals;

    /**
     * constructor
     * @param local_name nom de l'equip local
     * @param local_goals nombre de gols de l'equip local
     * @param visitor_name nom de l'equip visitant
     * @param visitor_goals nombre de gols de l'equip visitant
     */
    public Match(String local_name, int local_goals, String visitor_name, int visitor_goals) {
        this.local_name = local_name;
        this.local_goals = local_goals;
        this.visitor_name = visitor_name;
        this.visitor_goals = visitor_goals;
    }

    /**
     * constructor
     * @param id id del partit
     * @param local_name nom de l'equip local
     * @param local_goals nombre de gols de l'equip local
     * @param visitor_name nom de l'equip visitant
     * @param visitor_goals nombre de gols de l'equip visitant
     */
    public Match(int id, String local_name, int local_goals, String visitor_name, int visitor_goals) {
        this.id = id;
        this.local_name = local_name;
        this.local_goals = local_goals;
        this.visitor_name = visitor_name;
        this.visitor_goals = visitor_goals;
    }

    // Getters
    public int getId() {
        return id;
    }

    public String getLocal_name() {
        return local_name;
    }

    public int getLocal_goals() {
        return local_goals;
    }

    public String getVisitor_name() {
        return visitor_name;
    }

    public int getVisitor_goals() {
        return visitor_goals;
    }
}
