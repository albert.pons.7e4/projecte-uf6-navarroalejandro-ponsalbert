package models;

public class Team {
    //variables de la classe
    private int id;
    private String name;
    private String abv;

    /**
     * constructor
     * @param id id de l'equip
     * @param name nom de l'equip
     * @param abv abreviacio de l'equip
     */
    public Team(int id, String name, String abv) {
        this.id = id;
        this.name = name;
        this.abv = abv;
    }

    /**
     * constructor
     * @param name nom de l'equip
     * @param abv abreviacio de l'equip
     */
    public Team(String name, String abv) {
        this.name = name;
        this.abv = abv;
    }
    // Getters
    public int getId() {
        return id;
    }
    public String getName() {
        return name;
    }
    public String getAbv() {
        return abv;
    }

    @Override
    public String toString() {
        return String.format("%s (%s)", name, abv);
    }
}
