package data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Database {
    //informacio de la bdd
    private static String URL = "jdbc:postgresql://rogue.db.elephantsql.com:5432/";
    private static String BD = "xklugrlh";
    private static String USER = "xklugrlh";
    private static String PASS = "yBiIq5UTX4DzaJ2kmas1KXtCBFGHnEAv";

    //instancia de la bdd
    private static Database database = null;
    public static Database getInstance(){
        if(database == null)
            database = new Database();
        return database;
    }

    private Connection connection;

    //constructor
    public Database(){
        connection = null;
    }

    //metode per establir la conexio amb la bdd
    public Connection connect(){
        try {
            connection = DriverManager.getConnection(URL + BD, USER, PASS);
            System.out.println("La base de dades s'ha iniciat correctament.");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return connection;
    }

    //metode per retornar una conexio establerta
    public Connection getConnection() {
        return connection;
    }

    //metode per terminar la conexio amb la bdd
    public void close(){
        try {
            connection.close();
            System.out.println("La base de dades s'ha tancat correctament.");
        }catch(SQLException e){
            System.err.println("Error tancant la BD");
        }
        connection = null;
    }

}
