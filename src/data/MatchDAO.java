package data;

import exceptions.MatchNotFoundException;
import models.Match;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class MatchDAO implements DAO<Match>{

    //Retorna la llista de partits de la bdd
    @Override
    public List<Match> list() {
        List<Match> list = new ArrayList<>();
        try{
            Connection connection = Database.getInstance().getConnection();
            String query = "SELECT * FROM matches ORDER BY match_id";
            Statement listStatement = connection.createStatement();
            ResultSet resultat = listStatement.executeQuery(query);
            while (resultat.next()){
                int id = resultat.getInt("match_id");
                String local_name = resultat.getString("local_name");
                int local_goals = resultat.getInt("local_goals");
                String visitor_name = resultat.getString("visitor_name");
                int visitor_goals = resultat.getInt("visitor_goals");

                list.add(new Match(id, local_name, local_goals, visitor_name, visitor_goals));
            }
            // connection.close();
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return list;
    }

    //Permet inserir un nou partit a la bdd
    @Override
    public void insert(Match m) {
        try {
            Connection connection = Database.getInstance().getConnection();
            String query = "INSERT INTO matches (local_name, local_goals, visitor_name, visitor_goals) values (?, ?, ?, ?)";
            PreparedStatement insertStatement = connection.prepareStatement(query);
            insertStatement.setString(1, m.getLocal_name());
            insertStatement.setInt(2, m.getLocal_goals());
            insertStatement.setString(3, m.getVisitor_name());
            insertStatement.setInt(4, m.getVisitor_goals());
            insertStatement.execute();
            System.out.println("Partit insertat correctament");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    //Permet actualitzar un partit a la bdd
    @Override
    public boolean update(Match m) {
        try {
            Connection connection = Database.getInstance().getConnection();
            String query = "UPDATE matches SET(local_name, local_goals, visitor_name, visitor_goals) = (?, ?, ?, ?) WHERE match_id = ?";
            PreparedStatement insertStatement = connection.prepareStatement(query);
            insertStatement.setString(1, m.getLocal_name());
            insertStatement.setInt(2, m.getLocal_goals());
            insertStatement.setString(3, m.getVisitor_name());
            insertStatement.setInt(4, m.getVisitor_goals());
            insertStatement.setInt(5, m.getId());
            insertStatement.execute();
            System.out.println("Partit actualitzat correctament");
            return true;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return false;
    }

    //Permet esborrar un partit a la bdd
    @Override
    public boolean delete(int id) {
        try{
            Connection connection = Database.getInstance().getConnection();
            String query = "DELETE FROM matches WHERE match_id = ?";
            PreparedStatement insertStatement = connection.prepareStatement(query);
            insertStatement.setInt(1, id);
            insertStatement.execute();
            return true;
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return false;
    }

    //Et retorna un sol partit
    @Override
    public Match get(int id) throws MatchNotFoundException {
        Match m = null;
        try{
            Connection connection = Database.getInstance().getConnection();
            String query = "SELECT * FROM matches WHERE match_id = ?";
            PreparedStatement insertStatement = connection.prepareStatement(query);
            insertStatement.setInt(1, id);
            ResultSet result = insertStatement.executeQuery();
            if(!result.next())
                throw new MatchNotFoundException("No s'ha trobat el partit amb id: "+ id);

            String name1 = result.getString("local_name");
            int goals1 = result.getInt("local_goals");
            String name2 = result.getString("visitor_name");
            int goals2 = result.getInt("visitor_goals");
            m = new Match(id, name1, goals1, name2, goals2);

        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return m;
    }
}
