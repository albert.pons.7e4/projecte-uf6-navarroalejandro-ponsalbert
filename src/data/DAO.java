package data;

import java.util.List;

public interface DAO<K>{
    //llista de metodes obligatoris per les classes DAO

    List<K> list();
    void insert(K k);
    boolean update(K k);
    boolean delete(int id);
    K get(int id) throws Exception;
}
