package data;

import exceptions.TeamNotFoundException;
import models.Team;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class TeamDAO implements DAO<Team>{

    //Et retorna la llista de equips de la bdd
    @Override
    public List<Team> list(){
        List<Team> list = new ArrayList<>();
        try{
            Connection connection = Database.getInstance().getConnection();
            String query = "SELECT * FROM teams ORDER BY team_id";
            Statement listStatement = connection.createStatement();
            ResultSet resultat = listStatement.executeQuery(query);
            while (resultat.next()){
                int id = resultat.getInt("team_id");
                String name = resultat.getString("name");
                String abv = resultat.getString("abv");

                list.add(new Team(id, name, abv));
            }
            // connection.close();
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return list;
    }

    //Et permet inserir un nou equip a la bdd
    @Override
    public void insert(Team t) {
        try {
            Connection connection = Database.getInstance().getConnection();
            String query = "INSERT INTO teams (name, abv) values (?, ?)";
            PreparedStatement insertStatement = connection.prepareStatement(query);
            insertStatement.setString(1, t.getName());
            insertStatement.setString(2, t.getAbv());
            insertStatement.execute();
            System.out.println("Equip insertat correctament");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    //Et permet actualitzar un partit a la bdd
    public boolean update(Team team) {
        try{
            Connection connection = Database.getInstance().getConnection();
            String query = "UPDATE teams SET(name, abv) = (?, ?) WHERE team_id = ?";
            PreparedStatement insertStatement = connection.prepareStatement(query);
            insertStatement.setString(1, team.getName());
            insertStatement.setString(2, team.getAbv());
            insertStatement.setInt(3, team.getId());
            insertStatement.execute();
            System.out.println("Partit actualitzat correctament");
            return true;
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return false;

    }

    //Et permet suprimir un equip a la bdd
    @Override
    public boolean delete(int id) {
        try{
            Connection connection = Database.getInstance().getConnection();
            String query = "DELETE FROM teams WHERE team_id = ?";
            PreparedStatement insertStatement = connection.prepareStatement(query);
            insertStatement.setInt(1, id);
            insertStatement.execute();
            return true;
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return false;
    }

    //Et retorna un equip de la bdd
    @Override
    public Team get(int id) throws TeamNotFoundException{
        Team t = null;
        try{
            Connection connection = Database.getInstance().getConnection();
            String query = "SELECT * FROM teams WHERE team_id = ?";
            PreparedStatement insertStatement = connection.prepareStatement(query);
            insertStatement.setInt(1, id);
            ResultSet result = insertStatement.executeQuery();
            if(!result.next())
                throw new TeamNotFoundException("No s'ha trobat l'equip amb id: "+ id);

            String name = result.getString("name");
            String abv = result.getString("abv");
            t = new Team(id, name, abv);
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return t;
    }
}
