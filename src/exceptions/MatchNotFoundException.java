package exceptions;

public class MatchNotFoundException extends Exception{
    //Classe per controlar les excepcions dels partits

    public MatchNotFoundException() {
        super();
    }

    public MatchNotFoundException(String message) {
        super(message);
    }
}
