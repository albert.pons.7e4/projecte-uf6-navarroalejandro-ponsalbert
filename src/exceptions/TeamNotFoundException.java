package exceptions;

public class TeamNotFoundException extends Exception{
    //Classe per controlar les excepcions dels equips

    public TeamNotFoundException() {
        super();
    }

    public TeamNotFoundException(String message) {
        super(message);
    }
}
