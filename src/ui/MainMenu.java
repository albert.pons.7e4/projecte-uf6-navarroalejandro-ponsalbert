package ui;

import data.MatchDAO;
import data.TeamDAO;
import exceptions.MatchNotFoundException;
import exceptions.TeamNotFoundException;
import models.Match;
import models.Team;
import java.util.List;
import static ui.Printer.*;

public class MainMenu {
    private final MyScanner scanner;
    private final TeamDAO teamDAO;
    private final MatchDAO matchDAO;

    //contructor passant-li scanner
    public MainMenu(MyScanner scanner) {
        this.scanner = scanner;
        this.teamDAO = new TeamDAO();
        this.matchDAO = new MatchDAO();
    }

    //metode menu principal
    public void showMenu (){
        while(true){
            Printer.printBetweenLines("MENÚ LLIGA FUTBOL 2021");
            System.out.println("Què vols executar?\n");
            System.out.println("1) Insertar");
            System.out.println("2) Actualitzar");
            System.out.println("3) Esborrar");
            System.out.println("4) Mostrar");
            System.out.println();
            System.out.println("0) Exit");
            int accio = scanner.maxNum(4);
            switch (accio){
                case 1:
                    insertar();
                    break;
                case 2:
                    update();
                    break;
                case 3:
                    esborrar();
                    break;
                case 4:
                    mostrar();
                    break;
                case 0:
                    return;
            }
        }
    }

    //menu per esborrar
    private void esborrar() {
        while (true) {
            printBetweenLines("ESBORRAR");
            System.out.println("On vols esborrar?\n");
            System.out.println("1) Taula d'equips");
            System.out.println("2) Taula de partits");
            System.out.println();
            System.out.println("0) Enrere");
            int accio = scanner.maxNum(2);
            switch (accio) {
                case 1:
                    listTeams();
                    deleteTeam();
                    break;
                case 2:
                    listMatches();
                    deleteMatch();
                    break;
                case 0:
                    return;
            }
        }
    }

    //metode per esborrar un partit
    private void deleteMatch() {
        System.out.println("Selecciona l'id a esborrar:");
        int id = scanner.nextInt();
        matchDAO.delete(id);
    }

    //metode per esborrar un equip
    private void deleteTeam() {
        System.out.println("Selecciona l'id a esborrar:");
        int id = scanner.nextInt();
        teamDAO.delete(id);
    }

    //menu per mostrar o printar taules i dades
    private void mostrar() {
        while (true) {
            printBetweenLines("MOSTRAR");
            System.out.println("Què vols mostrar?\n");
            System.out.println("1) Taula d'equips");
            System.out.println("2) Taula de partits");
            System.out.println("3) Tria equip");
            System.out.println("4) Tria partit");
            System.out.println();
            System.out.println("0) Enrere");
            int accio = scanner.maxNum(4);
            switch (accio) {
                case 1:
                    listTeams();
                    break;
                case 2:
                    listMatches();
                    break;
                case 3:
                    getTeam();
                    break;
                case 4:
                    getMatch();
                    break;
                case 0:
                    return;
            }
        }
    }

    //metode per mostrar un equip concret
    private void getTeam(){
        listTeams();
        System.out.println("Selecciona l'equip a mostrar: ");
        int id = scanner.nextInt();
        try {
            Team t = teamDAO.get(id);
            printTeam(t);
        } catch (TeamNotFoundException e) {
            e.printStackTrace();
        }
    }

    //metode per mostrar un partit concret
    private void getMatch(){
        listMatches();
        System.out.println("Selecciona el partit a mostrar: ");
        int id = scanner.nextInt();
        try {
            Match m = matchDAO.get(id);
            printMatch(m);
        } catch (MatchNotFoundException e) {
            e.printStackTrace();
        }
    }

    //metode per printar la llista de partits
    private void listMatches() {
        List<Match> matches = matchDAO.list();
        printMatchesTable(matches);
    }

    //metode per printar la llista de equips
    private void listTeams() {
        List<Team> teams = teamDAO.list();
        printTeamTable(teams);
    }

    //menu per instertat dades
    private void insertar() {
        while (true) {
            printBetweenLines("INSERTAR");
            System.out.println("On vols insertar?");
            System.out.println();
            System.out.println("1) Taula d'equips");
            System.out.println("2) Taula de partits");
            System.out.println();
            System.out.println("0) Enrere");
            int accio = scanner.maxNum(2);
            switch (accio) {
                case 1:
                    addTeam();
                    break;
                case 2:
                    addMatch();
                    break;
                case 0:
                    return;
            }
        }
    }

    //metode per insertat un nou partit
    private void addMatch() {
        listTeams();
        System.out.println("Indica quin id de equip vols introduir com a local");
        int id1 = scanner.nextInt();
        System.out.println("Indica quin id de equip vols introduir com a visitant");
        int id2 = scanner.nextInt();
        String name1 = null;
        String name2 = null;

        for (int i = 0; i < teamDAO.list().size(); i++) {
            if (teamDAO.list().get(i).getId() == id1)
                name1 = teamDAO.list().get(i).getName();
            if (teamDAO.list().get(i).getId() == id2)
                name2 = teamDAO.list().get(i).getName();
        }
        System.out.println("Introdueix els gols del " + name1);
        int goals1 = scanner.nextInt();
        System.out.println("Introdueix els gols del " + name2);
        int goals2 = scanner.nextInt();
        Match m = new Match(name1, goals1, name2, goals2);
        matchDAO.insert(m);
    }

    //metode per insertat un nou equip
    private void addTeam() {
        System.out.println("Introdueix el nom");
        String name = scanner.next();
        System.out.println("Introdueix l'abreviació");
        String abv = scanner.next();
        Team t = new Team(name, abv);
        teamDAO.insert(t);
    }

    //menu dels updates
    private void update() {
        while (true) {
            printBetweenLines("ACTUALITZAZR");
            System.out.println("Que vols actualitzar?");
            System.out.println();
            System.out.println("1) Taula d'equips");
            System.out.println("2) Taula de partits");
            System.out.println();
            System.out.println("0) Enrere");
            int accio = scanner.maxNum(2);
            switch (accio) {
                case 1:
                    updateTeam();
                    break;
                case 2:
                    updateMatch();
                    break;
                case 0:
                    return;
            }
        }
    }

    //metode per actualitzar un equip
    private void updateTeam() {
        listTeams();
        System.out.println("Indica l'id de l'equip que vols actualitzar");
        int id = scanner.nextInt();
        System.out.println("Introdueix el nou nom");
        String name = scanner.next();
        System.out.println("Introdueix la nova abreviació");
        String abv = scanner.next();
        Team t = new Team(id,name,abv);
        teamDAO.update(t);
    }

    //metode per actualitzar un partit
    private void updateMatch() {
        listMatches();
        System.out.println("Indica l'id del partit que vols actualitzar");
        int id = scanner.nextInt();

        listTeams();
        System.out.println("Selecciona l'id del nou l'equip local");
        int name1_id = scanner.nextInt();
        String name1 = null;
        for (Team t : teamDAO.list()) {
            if (t.getId() == name1_id)
                name1 = t.getName();
        }

        System.out.println("Introdueix els nous gols de l'equip local");
        int goals1 = scanner.nextInt();

        listTeams();
        System.out.println("Selecciona l'id del nou l'equip visitant");
        int name2_id = scanner.nextInt();
        String name2 = null;
        for (Team t : teamDAO.list()) {
            if (t.getId() == name2_id)
                name2 = t.getName();
        }

        System.out.println("Introdueix els nous gols de l'equip visitant");
        int goals2 = scanner.nextInt();

        Match m = new Match(id,name1,goals1,name2,goals2);
        matchDAO.update(m);
    }
}