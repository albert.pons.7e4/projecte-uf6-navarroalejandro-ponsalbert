package ui;

import models.Match;
import models.Team;

import java.util.List;

// Herramientas esteticas de impresion
public class Printer {

    // Subclass for formatting text
    public static class Formatter {
        public static final String RESET = "\033[0m";       // RESET
        public static final String BOLD = "\033[1m";        // BOLD
        public static final String PURPLE = "\033[0;35m";   // PURPLE
        public static final String YELLOW = "\033[0;33m";   // YELLOW
        public static final String BLUE = "\033[0;34m";     // BLUE
    }

    /**
     * imprimeix una cadena de caracters entre duess barres
     * @param string cadena de caracters a imprimir
     */
    public static void printBetweenLines(String string){
        StringBuilder line= new StringBuilder();
        int lineSize = string.length()+8;
        for (int i = 0; i < lineSize; i++) {
            line.append("-");
        }
        //System.out.println(line+"\n"+"    "+string+"\n"+line);
        System.out.println(line+"\n\t"+string+"\n"+line);
    }

    //metode per printar la taula d'equips amb format
    public static void printTeamTable(List<Team> teams) {
        System.out.print(Formatter.BLUE);
        System.out.println("┏━━━━┳━━━━━━━━━━━━━━┳━━━━━━━━━┓");
        System.out.print(Formatter.BOLD);
        System.out.println("┃ ID ┃    TEAM      ┃   ABV   ┃");
        System.out.print(Formatter.RESET);
        System.out.print(Formatter.BLUE);
        System.out.println("┡━━━━╇━━━━━━━━━━━━━━╇━━━━━━━━━┩");

        for (int i = 0; i < teams.size(); i++) {
            Team t = teams.get(i);
            if (i > 0) {
                System.out.println("├────┼──────────────┼─────────┤");
            }
            System.out.printf("│ "+Formatter.RESET+"#%d "+Formatter.BLUE +"│" +
                            Formatter.RESET+"\t%-12s"+Formatter.BLUE +"│" +
                            Formatter.RESET+"\t%-6s"+Formatter.BLUE +"│\n",
                    t.getId(), t.getName(), t.getAbv());
        }
        System.out.println("└────┴──────────────┴─────────┘");
        System.out.print(Formatter.RESET);
    }

    //metode per printar la taula de partits amb format
    public static void printMatchesTable(List<Match> matches) {
        System.out.print(Formatter.BLUE);
        System.out.println("┏━━━━┳━━━━━━━━━━━━━━┳━━━━━━━┳━━━━━━━━━━━━━━━┳━━━━━━━┓");
        System.out.print(Formatter.BOLD);
        System.out.println("┃ ID ┃    LOCAL     ┃ SCORE ┃    VISITOR    ┃ SCORE ┃");
        System.out.print(Formatter.RESET);
        System.out.print(Formatter.BLUE);
        System.out.println("┡━━━━╇━━━━━━━━━━━━━━╇━━━━━━━╇━━━━━━━━━━━━━━━╇━━━━━━━┩");

        for (int i = 0; i < matches.size(); i++) {
            Match m = matches.get(i);
            if (i > 0) {
                System.out.println("├────┼──────────────┼───────┼───────────────┼───────┤");
            }
            System.out.printf("│ "+Formatter.RESET+"#%d "+Formatter.BLUE +"│" +
                            Formatter.RESET+"\t%-12s"+Formatter.BLUE +"│" +
                            Formatter.RESET+"\t%d\t"+Formatter.BLUE +"│" +
                            Formatter.RESET+"\t%-12s"+Formatter.BLUE +"│" +
                            Formatter.RESET+"\t%d\t"+Formatter.BLUE +"│\n",
                    m.getId(), m.getLocal_name(), m.getLocal_goals(), m.getVisitor_name(), m.getVisitor_goals());
        }
        System.out.println("└────┴──────────────┴───────┴───────────────┴───────┘");
        System.out.print(Formatter.RESET);
    }

    //metode per printar un partit amb format
    public static void printMatch(Match m) {
        System.out.print(Formatter.BLUE);
        System.out.println("┏━━━━┳━━━━━━━━━━━━━━┳━━━━━━━┳━━━━━━━━━━━━━━━┳━━━━━━━┓");
        System.out.print(Formatter.BOLD);
        System.out.println("┃ ID ┃    LOCAL     ┃ SCORE ┃    VISITOR    ┃ SCORE ┃");
        System.out.print(Formatter.RESET);
        System.out.print(Formatter.BLUE);
        System.out.println("┡━━━━╇━━━━━━━━━━━━━━╇━━━━━━━╇━━━━━━━━━━━━━━━╇━━━━━━━┩");

        System.out.printf("│ "+Formatter.RESET+"#%d "+Formatter.BLUE +"│" +
                        Formatter.RESET+"\t%-12s"+Formatter.BLUE +"│" +
                        Formatter.RESET+"\t%d\t"+Formatter.BLUE +"│" +
                        Formatter.RESET+"\t%-12s"+Formatter.BLUE +"│" +
                        Formatter.RESET+"\t%d\t"+Formatter.BLUE +"│\n",
                m.getId(), m.getLocal_name(), m.getLocal_goals(), m.getVisitor_name(), m.getVisitor_goals());

        System.out.println("└────┴──────────────┴───────┴───────────────┴───────┘");
        System.out.print(Formatter.RESET);
    }

    //metode per printar un equip amb format
    public static void printTeam(Team t) {
        System.out.print(Formatter.BLUE);
        System.out.println("┏━━━━┳━━━━━━━━━━━━━━┳━━━━━━━━━┓");
        System.out.print(Formatter.BOLD);
        System.out.println("┃ ID ┃    TEAM      ┃   ABV   ┃");
        System.out.print(Formatter.RESET);
        System.out.print(Formatter.BLUE);
        System.out.println("┡━━━━╇━━━━━━━━━━━━━━╇━━━━━━━━━┩");

        System.out.printf("│ "+Formatter.RESET+"#%d "+Formatter.BLUE +"│" +
                        Formatter.RESET+"\t%-12s"+Formatter.BLUE +"│" +
                        Formatter.RESET+"\t%-6s"+Formatter.BLUE +"│\n",
                t.getId(), t.getName(), t.getAbv());

        System.out.println("└────┴──────────────┴─────────┘");
        System.out.print(Formatter.RESET);
    }
}
