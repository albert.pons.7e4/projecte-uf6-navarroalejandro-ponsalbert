import data.Database;
import ui.MainMenu;
import ui.MyScanner;

public class LligaApp {
    public static void main(String[] args) {
        //classe main per executar el programa
        Database db = Database.getInstance();
        db.connect();

        MyScanner scanner = new MyScanner();
        MainMenu menu = new MainMenu(scanner);
        menu.showMenu();

        db.close();
    }
}
